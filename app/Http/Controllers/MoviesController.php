<?php

namespace App\Http\Controllers;

use App\Http\Requests\SaveMovieRequest;
use App\Movie;
use Illuminate\Http\Request;

use App\Http\Requests;

class MoviesController extends Controller
{
    public function index()
    {
        $movies = Movie::all();
        return view('movies.index', compact('movies'));
    }

    public function show($id)
    {
        $movie = Movie::findOrFail($id);
        return view('movies.show', compact('movie'));
    }

    public function create()
{
    return view('movies.create');
}

    public function store(SaveMovieRequest $request)
    {
        $movie = new Movie();
        $movie->title = $request->title;
        $movie->director = $request->director;
        $movie->actors = $request->actors;
        $movie->runtime = $request->runtime;
        $movie->genre = $request->genre;
        $movie->knt = $request->knt ? true : false;
        $movie->save();
        return redirect(route('movies.show', $movie->id));
    }

    public function edit($id)
    {
        $movie = Movie::findOrFail($id);
        return view('movies.edit', compact('movie'));
    }

    public function update($id, SaveMovieRequest $request)
    {
        $movie = Movie::findOrFail($id);
        $movie->title = $request->title;
        $movie->director = $request->director;
        $movie->actors = $request->actors;
        $movie->runtime = $request->runtime;
        $movie->genre = $request->genre;
        $movie->knt = $request->knt ? true : false;
        $movie->save();
        return redirect(route('movies.show', $movie->id));
    }

    public function delete($id)
    {
        $movie = Movie::findOrFail($id);
        $movie->delete();
        return redirect(route('home'));
    }
}
