<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);

        DB::truncate('movies');

        $movie = new Movie();
        $movie->title = 'Star Wars: Rogue One';
        $movie->director = 'Gareth Edwards';
        $movie->actors = 'Felicity Jones, Mads Mikkelsen, Ben Mendelsohn';
        $movie->runtime = '120';
        $movie->genre = 'action';
        $movie->knt = false;
        $movie->save();

        $movie = new Movie();
        $movie->title = 'Star Wars: Episode VII - The Force Awakens';
        $movie->director = 'J.J. Abrams';
        $movie->actors = 'Harrison Ford, Mark Hamill, Carrie Fisher, Adam Driver, Daisy Ridley';
        $movie->runtime = '120';
        $movie->genre = 'fantasy';
        $movie->knt = false;
        $movie->save();
    }
}
