MOVIES OPDRACHT
------
- Views: Index , Toevoegen, Aanpassen, Verwijderen

DATABANK VELDEN
------

- Title
- Director
- Actors
- Runtime (in min)
- Genre
- KNT

STAPPEN
-------

- Nieuw project te installeren
- databank configureren
- migration maken
- model maken
- controller maken
- routes instellen
- controller functie code schrijven
- views voor HTML