@include('shared.errors')

<p class="form-group">
    {!! Form::label('title') !!} <br>
    {!! Form::text('title', null, ['class'=>'form-control']) !!}
</p>
<p class="form-group">
    {!! Form::label('director') !!} <br>
    {!! Form::text('director', null, ['class'=>'form-control']) !!}
</p>
<p class="form-group">
    {!! Form::label('actors') !!} <br>
    {!! Form::text('actors', null, ['class'=>'form-control']) !!}
</p>
<p class="form-group">
    {!! Form::label('runtime') !!} <br>
    {!! Form::text('runtime', null, ['class'=>'form-control']) !!}
</p>
<p class="form-group">
    {!! Form::label('genre') !!} <br>
    {!! Form::select('genre', [
        'action'=>'Action',
        'fantasy'=>'Fantasy',
        'comedy'=>'Comedy'
    ], null, ['class'=>'form-control']) !!}
</p>
<p class="checkbox">
    <label>
        {!! Form::checkbox('knt') !!} KNT
    </label>
</p>
<p class="form-group">
    {!! Form::submit($btnText, ['class'=>"btn btn-success"]) !!}
</p>
