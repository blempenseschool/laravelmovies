@extends('layouts.main')

@section('title', 'Index page')

@section('content')

    <h1>Movies</h1>

    <p>
        <a href="{{ route('movies.create') }}" class="btn btn-primary">Create new movie &raquo;</a>
    </p>

    @if($movies->isEmpty())
        <p>No movies yet.</p>
    @else
        <table class="table">
            <tr>
                <th>Title</th>
                <th>Genre</th>
                <th>Runtime</th>
                <th>KNT</th>
            </tr>
            @foreach($movies as $movie)
            <tr>
                <td><a href="{{ route('movies.show', $movie->id) }}">{{ $movie->title }}</a></td>
                <td>{{ $movie->genre }}</td>
                <td>{{ $movie->runtime }}min</td>
                <td>{{ $movie->knt ? 'YES' : 'NO' }}</td>
            </tr>
            @endforeach
        </table>
    @endif
@endsection