@extends('layouts.main')

@section('title', $movie->title)

@section('content')

    <h1>{{ $movie->title }}</h1>

    <p>
        <a href="{{ route('home') }}" class="btn btn-primary">&laquo; Back to homepage</a>
    </p>

    <hr>
    <p>
        <a href="{{ route('movies.edit', $movie->id) }}" class="btn btn-warning">Edit</a>
        <a href="{{ route('movies.delete', $movie->id) }}" class="btn btn-danger">Delete</a>
    </p>
    <hr>


    <p>
        <strong>Directed by: </strong> {{ $movie->director }}
    </p>

    <p>
        <strong>Runtime: </strong> {{ $movie->runtime }}min
    </p>

    <p>
        <strong>Actors: </strong>
    </p>
    <ul>
        @foreach(explode(',', $movie->actors) as $actor)
            <li>{{ $actor }}</li>
        @endforeach
    </ul>

    <p>
        <strong>Genre: </strong> {{ $movie->genre }}
    </p>

    <p>
        Children: <strong>{{ $movie->knt ? 'Not allowed' : 'Allowed' }}</strong>
    </p>



@endsection