@extends('layouts.main')

@section('title', 'Update: ' . $movie->title)

@section('content')

    <h1>Edit movie: {{ $movie->title }}</h1>

    <p>
        <a href="{{ route('home') }}" class="btn btn-primary">&laquo; Back to homepage</a>
    </p>

    {!! Form::model($movie, ['route'=>['movies.update', $movie->id]]) !!}
    @include('movies.form', ['btnText'=>'Edit movie'])
    {!! Form::close() !!}

@endsection