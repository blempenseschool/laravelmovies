@extends('layouts.main')

@section('title', 'Create new movie')

@section('content')

    <h1>Create new movie</h1>

    <p>
        <a href="{{ route('home') }}" class="btn btn-primary">&laquo; Back to homepage</a>
    </p>

    {!! Form::open(['route'=>'movies.store']) !!}
        @include('movies.form', ['btnText'=>'Create new movie'])
    {!! Form::close() !!}

@endsection