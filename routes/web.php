<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', 'MoviesController@index')->name('home');

Route::get('create', 'MoviesController@create')->name('movies.create');
Route::post('store', 'MoviesController@store')->name('movies.store');

Route::get('{id}/edit', 'MoviesController@edit')->name('movies.edit');
Route::post('{id}/update', 'MoviesController@update')->name('movies.update');

Route::get('{id}/delete', 'MoviesController@delete')->name('movies.delete');

Route::get('{id}', 'MoviesController@show')->name('movies.show');
